**Product-Catalog Introduction**
In this small project, we have a catalog that contains many products. each product has many categories, also each category has many products.
The required work, is to use php to implement a simple backend  matches the requirements above.
From our point of view, product-catalog can be handled as a simple example of many-to-many relationship in REST architecture. 
We adopted this architecture and tried to implement it in the best possible manner.
In addition to that, we tried to use the most suitable design patterns, and tried to make our code compatible with PSR12 code style and Laravel naming conventions.
Also we implemented a simple user interface using L5-swagger to document and test the implemented backend.

---

## Used Tools and Frameworks

To implement this project we used the followings:

1. Laravel version 8.42.1.
2. Composer version 2.0.13.
3. mysql  version 15.1 Distrib.
4. jwt-auth version 1.0  [jwt-auth installation](https://jwt-auth.readthedocs.io/en/docs/laravel-installation/). 
5. L5-swagger version 8.x [L5-swagger installation](https://github.com/DarkaOnLine/L5-Swagger/wiki/Installation-&-Configuration).
---
To setup this project locally, kindly use the following [link](https://devmarketer.io/learn/setup-laravel-project-cloned-github-com/).
P.S. the project does not need any specific steps more than the steps that are clarified in the link.
## API Architecture

The API of product-catalog is a rest API, our design depends on the following pattern:
1. We have three resources: products, categories, and products-categories.
2. each resource can be manipulated using the CRUD operations: using the following actions: **index, show, store, update, destroy**.
3. in each of the actions above, the success result is the resource itself.
4. the resource itself contains its data in addition to links represents his direct relations with the other resources.
5. this design enables the front-end developer to navigate between the related resources, also it will be useful to represent the conceptual relation between the product and the category by the physical resource products-categories which enables us to store data in it for the expected future use.
6. each action result is one of the following three statuses:200:success,401:authontication/authorization issue, and 500 other issue(server error).
7. We can see that, our errors handling method is very simple, but it can be extended in the future to return other statuses (in addition to 401,500).
8. The full design of our API has been documented using L5-swagger and has been published to the following free host  [Api documentation](http://alaa-abou-ahmad.rf.gd/api/documentation?i=1) . please don't use this documentation for testing purposes, because our free host supports only post and get methods [as clarified here](https://forum.infinityfree.net/t/put-and-delete-methods/7533)


## Used Design Patterns

In this project we used the following design patterns:
**repository-service** design pattern: in this pattern, the resource controller actions are isolated from the business logic, and the business logic which is implemented in the services is isolated from the data access layer that is implemented in the repositories.
This thing increases the code modularity and reusability.
Also we used the **MVC** design pattern which is compatible with laravel and compatible with the adopted **repository-service** design pattern which makes the code more modular and reusable.

## Used Authentication and Authorization Model
We adopted a simple Authentication/Authorization model by using jwt-auth, in this model every authenticated user is also authorized to do any thing (We do not have user roles or privileges).
The model depends on generating a token to the user who has a valid credential (email, password). The toke expires in one hour and after that the user has to login again to get a new token.
 Also a new user can be registered, and he will get the token after the registration process. For more details about the adopted model, kindly follow the following [link](https://jwt-auth.readthedocs.io/en/docs/quick-start/).

## Testing
We have implemented some seeder classes to fill in the database with some data. Also we tested the endpoints manually using postman and the implemented L5-swagger UI. Some test cases have been implemented and written using PHPUnit, and some factory classes have been written to support those test cases.
Unfortunately, the test cases cover only the happy scenarios of the endpoints, and in the future we might implement the other scenarios. Also in the future some unit tests must be written to test the methods of the services classes and the repositories classes. Finally we have to mention that our application has passed the implemented test cases.