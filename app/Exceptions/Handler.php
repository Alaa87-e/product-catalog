<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * formating the unauthenticated error response
     *
     * @param  mixed $request
     * @param  mixed $exception
     * @return void
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['status' => 401, 'error' => 'Unauthenticated.'], 401);
        }
        return redirect()->guest('login');
    }
    /**
     * handling the exception to return to the clients only two types of errors: 500, 401
     *
     * @param  mixed $request
     * @param  mixed $e
     * @return void
     */
    public function render($request, Throwable  $e)
    {
        if (!($e instanceof AuthenticationException)) {
            return response()->json(['status' => 500, 'error' => $e->getMessage()], 500);
        }
        return parent::render($request, $e);
    }
}
