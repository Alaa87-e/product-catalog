<?php

namespace App\Http\Controllers\Api\Auth;

use App\Services\Api\Auth\AuthService;
use Illuminate\Http\Request;
use  App\Models\User;
use App\Http\Controllers\Controller;
use Exception;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @OA\Post(
     *     path="/api/register",
     *     summary="Register a new user",
     *     description="register a new user",
     *     operationId="authRegister",
     *     tags={"auth"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass user name, email and password",
     *         @OA\JsonContent(
     *             required={"name","email","password"},
     *             @OA\Property(property="name", type="string", format="string", example="user1@mail.com"),
     *             @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *             @OA\Property(property="password", type="string", format="password", example="PassWord12345")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Login done response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(property="data", type="object",
     *                 @OA\Property(
     *                     property="access_token",
     *                     type="string",
     *                     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9."
     *                 ),
     *                 @OA\Property(
     *                     property="token_type",
     *                     type="string",
     *                     example="bearer"
     *                 ),
     *                 @OA\Property(
     *                     property="expires_in",
     *                     type="integer",
     *                     example="3600"
     *                 )
     *             )
     *         )
     *     )
     * )
     */

    public function register(Request $request)
    {
        $data = $request->only(['name', 'email', 'password']);
        $result = ['status' => 200];
        try {
            $result['data'] = $this->authService->register($data);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    /**
     * @OA\Post(
     *     path="/api/login",
     *     summary="Sign in",
     *     description="Login by email, password",
     *     operationId="authLogin",
     *     tags={"auth"},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass user credentials",
     *         @OA\JsonContent(
     *             required={"email","password"},
     *             @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *             @OA\Property(property="password", type="string", format="password", example="PassWord12345")
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Wrong credentials response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="401"),
     *             @OA\Property(property="error", type="string", example="Unauthorized")
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Login done response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(property="data", type="object",
     *                 @OA\Property(
     *                     property="access_token",
     *                     type="string",
     *                     example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9."
     *                 ),
     *                 @OA\Property(
     *                     property="token_type",
     *                     type="string",
     *                     example="bearer"
     *                 ),
     *                 @OA\Property(
     *                     property="expires_in",
     *                     type="integer",
     *                     example="3600"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function login(Request $request)
    {
        $data = $request->only(['email', 'password']);
        $result = ['status' => 200];
        try {
            $loginResult = $this->authService->login($data);
            if (array_key_exists('error', $loginResult)) {
                $result = ['status' => 401, 'error' => $loginResult['error']];
            } else {
                $result['data'] =  $loginResult;
            }
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
}
