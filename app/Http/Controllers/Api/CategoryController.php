<?php

namespace App\Http\Controllers\Api;

use App\Services\CategoryService;
use Exception;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
        $this->middleware('auth:api');
    }
    /**
     * @OA\Get(
     *     path="/api/categories",
     *     summary="List all categories",
     *     description="List all the available categories",
     *     operationId="categoriesIndex",
     *     tags={"categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                     @OA\Property(
     *                         property="id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         example="BMW"
     *                     ),
     *                     @OA\Property(
     *                         property="category_products_api_link",
     *                         type="string",
     *                         example="http://127.0.0.1:8000/api/categories/1/products-categories"
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function index()
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->categoryService->getAll();
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Get(
     *     path="/api/categories/{id}",
     *     summary="Show category by id",
     *     description="Show category by id",
     *     operationId="categoriesShow",
     *     tags={"categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="ID of category to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="BMW"
     *                 ),
     *                 @OA\Property(
     *                     property="category_products_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/categories/1/products-categories"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function show($id)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->categoryService->getById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function create()
    {
        //
    }
    /**
     * @OA\Post(
     *     path="/api/categories",
     *     summary="Store a new category",
     *     description="Store a new category",
     *     operationId="categoriesStore",
     *     tags={"categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass category name",
     *         @OA\JsonContent(
     *             required={"name"},
     *             @OA\Property(
     *                 property="name", 
     *                 type="string",
     *	               format="string",
     *                 example="Volvo"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success respons",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="Volvo"
     *                 ),
     *                 @OA\Property(
     *                     property="category_products_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/categories/1/products-categories"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(['name']);
        $result = ['status' => 200];
        try {
            $result['data'] = $this->categoryService->saveCategoryData($data);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function edit(Category $category)
    {
        //
    }

    /**
     * @OA\Put(
     *     path="/api/categories/{id}",
     *     summary="Update a category",
     *     description="Update a category",
     *     operationId="categoriesUpdate",
     *     tags={"categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="category id to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="category new name",
     *         in="query",
     *         name="name",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="string"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="Volvo"
     *                 ),
     *                 @OA\Property(
     *                     property="category_products_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/categories/1/products-categories"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        $data = $request->only(['name']);
        $result = ['status' => 200];
        try {
            $result['data'] = $this->categoryService->updateCategory($data, $id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Delete(
     *     path="/api/categories/{id}",
     *     summary="Delete a category",
     *     description="Delete a category",
     *     operationId="categoriesDelete",
     *     tags={"categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="category id to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="Volvo"
     *                 ),
     *                 @OA\Property(
     *                     property="category_products_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/categories/1/products-categories"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function destroy($id)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->categoryService->deleteById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
}
