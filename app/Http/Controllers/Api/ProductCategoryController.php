<?php

namespace App\Http\Controllers\Api;

use App\Services\ProductCategoryService;
use Exception;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{
    protected $productCategoryService;

    public function __construct(ProductCategoryService $productCategoryService)
    {
        $this->productCategoryService = $productCategoryService;
        $this->middleware('auth:api');
    }
    /**
     * @OA\Get(
     *     path="/api/products-categories",
     *     summary="List all products-categories relations",
     *     description="List all products categories relations",
     *     operationId="productsCategoriesIndex",
     *     tags={"products-categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                     @OA\Property(
     *                         property="id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="product_id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="category_id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="product_api_link",
     *                         type="string",
     *                         example="http://127.0.0.1:8000/api/products/1"
     *                     ),
     *                     @OA\Property(
     *                         property="category_api_link",
     *                         type="string",
     *                         example="http://127.0.0.1:8000/api/categories/1"
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function index()
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productCategoryService->getAll();
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Get(
     *     path="/api/products-categories/{id}",
     *     summary="Show product-category relation by its id",
     *     description="Show product-category relation by its id",
     *     operationId="productscategoriesShow",
     *     tags={"products-categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="ID of product-category relation  to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="product_id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="category_id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="product_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/products/1"
     *                 ),
     *                 @OA\Property(
     *                     property="category_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/categories/1"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function show($id)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productCategoryService->getById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Get(
     *     path="/api/products/{productId}/products-categories",
     *     summary="Show product-category relations of a product",
     *     description="Show product-category relations of a product",
     *     operationId="productsCategoriesShowProductCategories",
     *     tags={"products-categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="ID of product which you want to return its product-category relations",
     *         in="path",
     *         name="productId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *                  *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                     @OA\Property(
     *                         property="id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="product_id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="category_id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="product_api_link",
     *                         type="string",
     *                         example="http://127.0.0.1:8000/api/products/1"
     *                     ),
     *                     @OA\Property(
     *                         property="category_api_link",
     *                         type="string",
     *                         example="http://127.0.0.1:8000/api/categories/1"
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function showProductCategories(int $productId)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productCategoryService->getProductCategories($productId);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    /**
     * @OA\Get(
     *     path="/api/categories/{categoryId}/products-categories",
     *     summary="Show product-category relations of a category",
     *     description="Show product-category relations of a category",
     *     operationId="productscategoriesShowCategoryProductss",
     *     tags={"products-categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="ID of category which you want to return its product-category relations",
     *         in="path",
     *         name="categoryId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *                  *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                     @OA\Property(
     *                         property="id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="product_id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="category_id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="product_api_link",
     *                         type="string",
     *                         example="http://127.0.0.1:8000/api/products/1"
     *                     ),
     *                     @OA\Property(
     *                         property="category_api_link",
     *                         type="string",
     *                         example="http://127.0.0.1:8000/api/categories/1"
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function showCategoryProducts(int $categoryId)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productCategoryService->getCategoryProducts($categoryId);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function create()
    {
        //
    }
    /**
     * @OA\Post(
     *     path="/api/products-categories",
     *     summary="Store a new product-category relation",
     *     description="Store a new product-category relation",
     *     operationId="productsCategoriesStore",
     *     tags={"products-categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass product id and category id",
     *         @OA\JsonContent(
     *             required={"product_id","category_id"},
     *             @OA\Property(
     *                 property="product_id", 
     *                 type="integer",
     *	               format="integer",
     *                 example="1"
     *             ),
     *             @OA\Property(
     *                 property="category_id", 
     *                 type="integer",
     *	               format="integer",
     *                 example="1"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success respons",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="product_id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="category_id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="product_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/products/1"
     *                 ),
     *                 @OA\Property(
     *                     property="category_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/categories/1"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(['product_id', 'category_id']);
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productCategoryService->saveProductCategoryData($data);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function edit(Category $productCategory)
    {
        //
    }
    /**
     * @OA\Put(
     *     path="/api/products-categories/{id}",
     *     summary="Update a product-category rlation",
     *     description="Update a product-category rlation",
     *     operationId="productsCategoriesUpdate",
     *     tags={"products-categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="product-category relation id to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="new product id",
     *         in="query",
     *         name="product_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="new category id",
     *         in="query",
     *         name="category_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="product_id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="category_id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="product_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/products/1"
     *                 ),
     *                 @OA\Property(
     *                     property="category_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/categories/1"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function update(Request $request, $id)
    {
        $data = $request->only([
            'product_id',
            'category_id'
        ]);
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productCategoryService->updateProductCategory($data, $id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    /**
     * @OA\Delete(
     *     path="/api/products-categories/{id}",
     *     summary="Delete a produc-category relation",
     *     description="Delete a produc-category relation",
     *     operationId="productscategoriesDelete",
     *     tags={"products-categories"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="product category relation id to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="product_id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="category_id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="product_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/products/1"
     *                 ),
     *                 @OA\Property(
     *                     property="category_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/categories/1"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function destroy($id)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productCategoryService->deleteById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
}
