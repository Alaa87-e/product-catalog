<?php

namespace App\Http\Controllers\Api;

use App\Services\ProductService;
use Exception;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
        $this->middleware('auth:api');
    }

    /**
     * @OA\Get(
     *     path="/api/products",
     *     summary="List all products",
     *     description="List all the available products",
     *     operationId="productsIndex",
     *     tags={"products"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(
     *                     @OA\Property(
     *                         property="id",
     *                         type="integer",
     *                         example="1"
     *                     ),
     *                     @OA\Property(
     *                         property="name",
     *                         type="string",
     *                         example="316i"
     *                     ),
     *                     @OA\Property(
     *                         property="price",
     *                         type="number",
     *                         example="20000"
     *                     ),
     *                     @OA\Property(
     *                         property="product_categories_api_link",
     *                         type="string",
     *                         example="http://127.0.0.1:8000/api/products/1/products-categories"
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function index()
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productService->getAll();
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
    /**
     * @OA\Get(
     *     path="/api/products/{id}",
     *     summary="Show product by id",
     *     description="Show product by id",
     *     operationId="productsShow",
     *     tags={"products"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="ID of product to return",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="316i"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="number",
     *                     example="20000"
     *                 ),
     *                 @OA\Property(
     *                     property="product_categories_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/products/1/products-categories"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function show(int $id)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productService->getById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function create()
    {
        //might be implemented later on
    }

    /**
     * @OA\Post(
     *     path="/api/products",
     *     summary="Store a new product",
     *     description="Store a new product",
     *     operationId="productsStore",
     *     tags={"products"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\RequestBody(
     *         required=true,
     *         description="Pass product name and price",
     *         @OA\JsonContent(
     *             required={"name","price"},
     *             @OA\Property(
     *                 property="name", 
     *                 type="string",
     *	               format="string",
     *                 example="316i"
     *             ),
     *             @OA\Property(
     *                 property="price", 
     *                 type="number",
     *	               format="number",
     *                 example="20000"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success respons",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="316i"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="number",
     *                     example="20000"
     *                 ),
     *                 @OA\Property(
     *                     property="product_categories_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/products/1/products-categories"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function store(Request $request)
    {
        $data = $request->only(['name', 'price']);
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productService->saveProductData($data);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    public function edit(Product $product)
    {
        //might be implemented later on
    }

    /**
     * @OA\Put(
     *     path="/api/products/{id}",
     *     summary="Update a product",
     *     description="Update a product",
     *     operationId="productsUpdate",
     *     tags={"products"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="product id to update",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="product new name",
     *         in="query",
     *         name="name",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         description="product new price",
     *         in="query",
     *         name="price",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             format="number"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="316i"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="number",
     *                     example="20000"
     *                 ),
     *                 @OA\Property(
     *                     property="product_categories_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/products/1/products-categories"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function update(Request $request, int $id)
    {
        $data = $request->only([
            'name',
            'price'
        ]);
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productService->updateProduct($data, $id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }

    /**
     * @OA\Delete(
     *     path="/api/products/{id}",
     *     summary="Delete a product",
     *     description="Delete a product",
     *     operationId="productsDelete",
     *     tags={"products"},
     *     security={{ "bearerAuth": {} }},
     *     @OA\Parameter(
     *         description="product id to delete",
     *         in="path",
     *         name="id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Expired or wrong authontication token",
     *         @OA\JsonContent(
     *             @OA\Property(
     *                 property="status",
     *       	       type="integer",
     *	               example="401"
     *             ),
     *             @OA\Property(
     *                 property="error",
     *             	   type="string",
     *                 example="Unauthenticated"
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Server Error",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="500"),
     *             @OA\Property(property="error", type="string", example="server error message")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Request success response",
     *         @OA\JsonContent(
     *             @OA\Property(property="status", type="integer", example="200"),
     *             @OA\Property(
     *                 property="data",
     *                 type="object",
     *                 @OA\Property(
     *                     property="id",
     *                     type="integer",
     *                     example="1"
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                     example="316i"
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     type="number",
     *                     example="20000"
     *                 ),
     *                 @OA\Property(
     *                     property="product_categories_api_link",
     *                     type="string",
     *                     example="http://127.0.0.1:8000/api/products/1/products-categories"
     *                 )
     *             )
     *         )
     *     )
     * )
     */
    public function destroy(int $id)
    {
        $result = ['status' => 200];
        try {
            $result['data'] = $this->productService->deleteById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }
        return response()->json($result, $result['status']);
    }
}
