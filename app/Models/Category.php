<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table = "categories";
    public $category_products_api_link = null;
    protected $appends = ['category_products_api_link'];
    protected $fillable = ['name'];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category');
    }
    public function getCategoryProductsAPiLinkAttribute($value)
    {
        return route('categories.category_products', ['categoryId' => $this->id]);
    }
}
