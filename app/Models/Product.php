<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public $product_categories_api_link = null;
    protected $appends = ['product_categories_api_Link'];
    protected $fillable = ['name', 'price'];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_category');
    }
    public function getProductCategoriesAPiLinkAttribute($value)
    {
        return route('products.product_categories', ['productId' => $this->id]);
    }
}
