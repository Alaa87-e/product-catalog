<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    use HasFactory;

    protected $table = "product_category";
    public $product_api_link = null;
    public $category_api_link = null;
    protected $appends = ['product_api_link', 'category_api_link'];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    public function getProductApiLinkAttribute($value)
    {
        return route('products.show', ['product' => $this->product_id]);
    }
    public function getCategoryApiLinkAttribute($value)
    {
        return route('categories.show', ['category' => $this->category_id]);
    }
}
