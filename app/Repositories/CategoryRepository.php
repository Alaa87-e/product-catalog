<?php

namespace App\Repositories;

use App\Models\Category;

class CategoryRepository
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function getAll()
    {
        return $this->category
            ->get();
    }

    public function getById($id)
    {
        return $this->category
            ->where('id', $id)
            ->firstOrFail();
    }

    public function save($data)
    {
        $category = new $this->category;
        $category->name = $data['name'];
        $category->save();
        return $category->fresh();
    }

    public function update($data, $id)
    {
        $category = $this->category->find($id);
        $category->name = $data['name'];
        $category->update();
        return $category;
    }

    public function delete($id)
    {
        $category = $this->category->find($id);
        $category->delete();
        return $category;
    }
}
