<?php

namespace App\Repositories;

use App\Models\ProductCategory;

class ProductCategoryRepository
{
    protected $productCategory;

    public function __construct(ProductCategory $productCategory)
    {
        $this->productCategory = $productCategory;
    }

    public function getAll()
    {
        return $this->productCategory
            ->get();
    }

    public function getById($id)
    {
        return $this->productCategory
            ->where('id', $id)
            ->firstOrFail();
    }

    public function getProductCategories($productId)
    {
        return $this->productCategory
            ->where('product_id', $productId)
            ->get();
    }

    public function getCategoryProducts($categoryId)
    {
        return $this->productCategory
            ->where('category_id', $categoryId)
            ->get();
    }

    public function save($data)
    {
        $productCategory = new $this->productCategory;
        $productCategory->product_id = (int)$data['product_id'];
        $productCategory->category_id = (int) $data['category_id'];
        $productCategory->save();
        return $productCategory->fresh();
    }

    public function update($data, $id)
    {
        $productCategory = $this->productCategory->find($id);
        $productCategory->product_id = (int) $data['product_id'];
        $productCategory->category_id = (int) $data['category_id'];
        $productCategory->update();
        return $productCategory;
    }

    public function delete($id)
    {
        $productCategory = $this->productCategory->find($id);
        $productCategory->delete();
        return $productCategory;
    }
}
