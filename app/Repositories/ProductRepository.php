<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function getAll()
    {
        return $this->product
            ->get();
    }

    public function getById($id)
    {
        return $this->product
            ->where('id', $id)
            ->firstOrFail();
    }

    public function save($data)
    {
        $product = new $this->product;
        $product->name = $data['name'];
        $product->price = (float)$data['price'];
        $product->save();
        return $product->fresh();
    }

    public function update($data, $id)
    {
        $product = $this->product->find($id);
        $product->name = $data['name'];
        $product->price = (float)$data['price'];
        $product->update();
        return $product;
    }

    public function delete($id)
    {
        $product = $this->product->find($id);
        $product->delete();
        return $product;
    }
}
