<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getAll()
    {
        //might be implemented later on
    }

    public function getById($id)
    {
        //might be implemented later on
    }

    public function save($data)
    {
        $user = new $this->user;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        $user->save();
        return $user->fresh();
    }

    public function update($data, $id)
    {
        //might be implemented later on
    }

    public function delete($id)
    {
        //might be implemented later on
    }
}
