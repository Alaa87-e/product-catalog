<?php

namespace App\Services\Api\Auth;

use App\Repositories\UserRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class AuthService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register($user)
    {
        $validator = Validator::make($user, ['name' => 'required', 'password' => 'required', 'email' => ['required', 'email']]);
        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        $user['password'] = bcrypt($user['password']);
        $user = $this->userRepository->save($user);
        $token = auth()->login($user);
        $result = $this->respondWithToken($token);
        return $result;
    }

    public function login($credentials)
    {
        $token = auth()->attempt($credentials);
        if (!$token = auth()->attempt($credentials)) {
            $result = ['error' => 'Unauthorized'];
            return $result;
        }
        $result = $this->respondWithToken($token);
        return $result;
    }

    protected function respondWithToken($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ];
    }
}
