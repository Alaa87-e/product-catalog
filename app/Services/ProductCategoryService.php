<?php

namespace App\Services;


use App\Repositories\ProductCategoryRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class ProductCategoryService
{
    protected $productCategoryRepository;

    public function __construct(ProductCategoryRepository $productCategoryRepository)
    {
        $this->productCategoryRepository = $productCategoryRepository;
    }

    public function getAll()
    {
        return $this->productCategoryRepository->getAll();
    }

    public function getById($id)
    {
        return $this->productCategoryRepository->getById($id);
    }
    public function getProductCategories($productId)
    {
        return $this->productCategoryRepository->getProductCategories($productId);
    }
    public function getCategoryProducts($categoryId)
    {
        return $this->productCategoryRepository->getCategoryProducts($categoryId);
    }

    public function saveProductCategoryData($data)
    {
        $validator = Validator::make($data, ['product_id' => ['required', 'integer'], 'category_id' => ['required', 'integer']]);
        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        return $this->productCategoryRepository->save($data);
    }

    public function updateProductCategory($data, $id)
    {
        $validator = Validator::make($data, ['product_id' => ['required', 'integer'], 'category_id' => ['required', 'integer']]);
        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        DB::beginTransaction();
        try {
            $productCategory = $this->productCategoryRepository->update($data, $id);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to update product-category data' . $e->getMessage());
        }
        DB::commit();
        return $productCategory;
    }

    public function deleteById($id)
    {
        DB::beginTransaction();
        try {
            $productCategory = $this->productCategoryRepository->delete($id);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to delete product-category data');
        }
        DB::commit();
        return $productCategory;
    }
}
