<?php

namespace App\Services;

use App\Repositories\ProductRepository;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class ProductService
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAll()
    {
        return $this->productRepository->getAll();
    }

    public function getById($id)
    {
        return $this->productRepository->getById($id);
    }

    public function saveProductData($data)
    {
        $validator = Validator::make($data, [
            'name' => 'required',
            'price' => ['required', 'numeric']
        ]);
        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        $result = $this->productRepository->save($data);
        return $result;
    }

    public function updateProduct($data, $id)
    {
        $validator = Validator::make($data, [
            'name' => 'required',
            'price' => ['required', 'numeric']
        ]);
        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }
        DB::beginTransaction();
        try {
            $product = $this->productRepository->update($data, $id);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to update product data');
        }
        DB::commit();
        return $product;
    }

    public function deleteById($id)
    {
        DB::beginTransaction();
        try {
            $product = $this->productRepository->delete($id);
        } catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
            throw new InvalidArgumentException('Unable to delete product data');
        }
        DB::commit();
        return $product;
    }
}
