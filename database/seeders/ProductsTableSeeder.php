<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([[
            'name' => '316i',
            'price' => '20000'
        ], [
            'name' => '320i',
            'price' => '25000'
        ], [
            'name' => '520i',
            'price' => '30000'
        ], [
            'name' => '740i',
            'price' => '50000'
        ], [
            'name' => 'a4',
            'price' => '20000'
        ], [
            'name' => 'a6',
            'price' => '40000'
        ], [
            'name' => 'a8',
            'price' => '50000'
        ]]);
    }
}
