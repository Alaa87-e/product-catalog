<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('categories', 'App\Http\Controllers\Api\CategoryController', [
    'names' => [
        'index' => 'categories.index',
        'show' => 'categories.show',
        'create' => 'categories.create',
        'store' => 'categories.store',
        'edit' => 'categories.edit',
        'update' => 'categories.update',
        'destroy' => 'categories.destroy'
    ]
]);
Route::resource('products', 'App\Http\Controllers\Api\ProductController', [
    'names' => [
        'index' => 'products.index',
        'show' => 'products.show',
        'create' => 'products.create',
        'store' => 'products.store',
        'edit' => 'products.edit',
        'update' => 'products.update',
        'destroy' => 'products.destroy'
    ]
]);
Route::resource('products-categories', 'App\Http\Controllers\Api\ProductCategoryController', [
    'names' => [
        'index' => 'products-categories.index',
        'show' => 'products-categories.show',
        'create' => 'products-categories.create',
        'store' => 'products-categories.store',
        'edit' => 'products-categories.edit',
        'update' => 'products-categories.update',
        'destroy' => 'products-categories.destroy'
    ]
]);
Route::get('/products/{productId}/products-categories', 'App\Http\Controllers\Api\ProductCategoryController@showProductCategories')->name('products.product_categories');;
Route::get('/categories/{categoryId}/products-categories', 'App\Http\Controllers\Api\ProductCategoryController@showCategoryProducts')->name('categories.category_products');
Route::post('/register', 'App\Http\Controllers\Api\Auth\AuthController@register');
Route::post('/login', ['as' => 'login', 'uses' => 'App\Http\Controllers\Api\Auth\AuthController@login']);
