<?php

namespace Tests\Feature;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class CategoryApiTest extends TestCase
{

    use RefreshDatabase;
    use WithFaker;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_store_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $formData = ['name' => $this->faker->name()];
        $this->post(route('categories.store'), $formData)->assertStatus(200)->assertJson(['data' => $formData]);
    }
    public function test_can_show_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $this->get(route('categories.show', $category->id))->assertStatus(200);
    }
    public function test_can_list_categories()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $this->get(route('categories.index'))->assertStatus(200);
    }
    public function test_can_update_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $product = Category::factory()->create();
        $newCategoryFormData = ['name' => $this->faker->name()];
        $this->put(route('categories.update', $product->id), $newCategoryFormData)->assertStatus(200)->assertJson(['data' => $newCategoryFormData]);;
    }
    public function test_can_destroy_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $product = Category::factory()->create();
        $this->delete(route('categories.destroy', $product->id))->assertStatus(200);
    }
}
