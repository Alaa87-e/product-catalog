<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;


class ProductApiTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function test_can_store_product()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $formData = ['name' => $this->faker->name(), 'price' => $this->faker->randomNumber(3)];
        $this->post(route('products.store'), $formData)->assertStatus(200)->assertJson(['data' => $formData]);
    }
    public function test_can_show_product()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $product = Product::factory()->create();
        $this->get(route('products.show', $product->id))->assertStatus(200);
    }
    public function test_can_list_products()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $product = Product::factory()->create();
        $this->get(route('products.index'))->assertStatus(200);
    }
    public function test_can_update_product()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $product = Product::factory()->create();
        $newProductFormData = ['name' => $this->faker->name(), 'price' => $this->faker->randomNumber(3)];
        $this->put(route('products.update', $product->id), $newProductFormData)->assertStatus(200)->assertJson(['data' => $newProductFormData]);;
    }
    public function test_can_destroy_product()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $product = Product::factory()->create();
        $this->delete(route('products.destroy', $product->id))->assertStatus(200);
    }
}
