<?php

namespace Tests\Feature;

use App\Models\ProductCategory;
use App\Models\Category;
use App\Models\product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class ProductCategoryApiTest extends TestCase
{

    use RefreshDatabase;
    use WithFaker;

    public function test_can_store_product_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $formData = ['product_id' => $product->id, 'category_id' => $category->id];
        $this->post(route('products-categories.store'), $formData)->assertStatus(200); //assertJson(['data' => $formData]);
    }
    public function test_can_show_product_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productCategory = ProductCategory::factory()->create(['product_id' => $product->id, 'category_id' => $category->id]);
        $this->get(route('products-categories.show', $productCategory->id))->assertStatus(200);
    }
    public function test_can_list_all_products_categories()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productCategory = ProductCategory::factory()->create(['product_id' => $product->id, 'category_id' => $category->id]);
        $this->get(route('products-categories.index'))->assertStatus(200);
    }
    public function test_can_list_products_categories_by_product_id()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productCategory = ProductCategory::factory()->create(['product_id' => $product->id, 'category_id' => $category->id]);
        $this->get(route('products.product_categories', $product->id))->assertStatus(200);
    }
    public function test_can_list_products_categories_by_category_id()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productCategory = ProductCategory::factory()->create(['product_id' => $product->id, 'category_id' => $category->id]);
        $this->get(route('categories.category_products', $category->id))->assertStatus(200);
    }
    public function test_can_update_product_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productCategory = ProductCategory::factory()->create(['product_id' => $product->id, 'category_id' => $category->id]);
        $newCategory = Category::factory()->create();
        $newProduct = Product::factory()->create();
        $newProductCategoryFormData = ['product_id' => $newProduct->id, 'category_id' => $newCategory->id];
        $this->put(route('products-categories.update', $productCategory->id), $newProductCategoryFormData)->assertStatus(200)->assertJson(['data' => $newProductCategoryFormData]);;
    }
    public function test_can_destroy_product_category()
    {
        $user = User::factory()->create();
        $this->actingAs($user, 'api');
        $category = Category::factory()->create();
        $product = Product::factory()->create();
        $productCategory = ProductCategory::factory()->create(['product_id' => $product->id, 'category_id' => $category->id]);
        $this->delete(route('products-categories.destroy', $productCategory->id))->assertStatus(200);
    }
}
